import org.apache.commons.math.complex.Complex;

/**
 * Classe para testar o método da varredura (LadderIterative) de
 * cálculo de fluxo de potência em sistemas de distribuição
 * 
 * O teste é configurado para o sistema de 32 barras (bus_32_1.in)
 * 
 * @author Filipe Saraiva
 *
 */
public class TestLadderIterative {

	public static void main(String[] args) {
		Complex tensaoNominal = new Complex(12660, 0);
		RedeEletrica rede = new RedeEletrica();
		
		LadderIterative fluxoDePotencia;
		
		rede.getRedeEletrica().get(24).set(28, Complex.ZERO);
		rede.getRedeEletrica().get(28).set(24, Complex.ZERO);
		
		rede.getRedeEletrica().get(32).set(17, Complex.ZERO);
		rede.getRedeEletrica().get(17).set(32, Complex.ZERO);
		
		rede.getRedeEletrica().get(8).set(14, Complex.ZERO);
		rede.getRedeEletrica().get(14).set(8, Complex.ZERO);
		
		rede.getRedeEletrica().get(7).set(20, Complex.ZERO);
		rede.getRedeEletrica().get(20).set(7, Complex.ZERO);
		
		rede.getRedeEletrica().get(11).set(21, Complex.ZERO);
		rede.getRedeEletrica().get(21).set(11, Complex.ZERO);
		
		fluxoDePotencia = new LadderIterative(0, tensaoNominal,
				rede.getRedeEletrica(), rede.getCargas());
		
		fluxoDePotencia.executarLadderIterative();
		System.out.println("Correntes Elétricas nas Barras:");
		fluxoDePotencia.imprimeCorrentesEletricasBarras();
		System.out.println("Correntes Tensões nas Barras:");
		fluxoDePotencia.imprimeTensoes();
		System.out.println("Perdas Totais: " +
				fluxoDePotencia.getPerdasEletricasTotais().getReal() / 1000 + " kW");
	}

}
