
public class TestRedeEletrica {

	public static void main(String[] args) {
		RedeEletrica rede = new RedeEletrica();
		
		System.out.println("Rede Elétrica");
		rede.imprimeRedeEletrica();
		System.out.println();
		
		System.out.println("Rede Elétrica - Resistência");
		rede.imprimeRedeResistencia();
		System.out.println();
		
		System.out.println("Rede Elétrica - Cargas");
		rede.imprimeCargas();
		System.out.println();
	}
}
