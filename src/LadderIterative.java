import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;

import org.apache.commons.math.complex.Complex;
import org.apache.commons.math.complex.ComplexFormat;
import org.apache.commons.math.complex.ComplexUtils;
import org.apache.commons.math.util.MathUtils;

/**
 * Método de Cálculo de Fluxo de Potência
 * para Sistemas de Distribuição Radiais
 * Ladder Iterative ou Método da Varredura
 * 
 * @author Filipe Saraiva
 */

public class LadderIterative {

	/**
	 * 
	 */
	private Integer numIteracao = new Integer(0);
	
	/**
	 * 
	 */
	private Integer noFeeder = new Integer(0);
	
	/**
	 * 
	 */
	private Complex tolerancia = Complex.ZERO;
	
	/**
	 * 
	 */
	private Complex tensaoNominal = Complex.ZERO;
	
	/**
	 * 
	 */
	private ArrayList<Integer> grausIteracao = new ArrayList<Integer>();
	
	/**
	 * 
	 */
	private ArrayList<Integer> graus = new ArrayList<Integer>();
	
	/**
	 * 
	 */
	private ArrayList<ArrayList<Complex>> matrizImpedancia = new ArrayList<ArrayList<Complex>>();
	
	/**
	 * 
	 */
	private ArrayList<Complex> vetorDemanda = new ArrayList<Complex>();
	
	/**
	 * 
	 */
	private ArrayList<ArrayList<Complex>> correntesEletricasTrechos = new ArrayList<ArrayList<Complex>>();
	
	/**
	 * 
	 */
	private ArrayList<Complex> correntesEletricasBarras = new ArrayList<Complex>();
	
	/**
	 * 
	 */
	private ArrayList<Complex> tensao = new ArrayList<Complex>();
	
	/**
	 * 
	 */
	private ArrayList<ArrayList<Complex>> perdasEletricas = new ArrayList<ArrayList<Complex>>();
	
	private Complex perdasEletricasTotais = Complex.ZERO;
	
	public LadderIterative(){
		
	}
	
	public LadderIterative(Integer noFeeder, Complex tensaoNominal,
			ArrayList<ArrayList<Complex>> matrizImpedancia, ArrayList<Complex> vetorDemanda) {
		super();
		this.noFeeder = noFeeder;
		this.tensaoNominal = tensaoNominal;
		this.tolerancia = tensaoNominal.multiply(new Complex(0.0001, 0));
		this.matrizImpedancia = matrizImpedancia;
		this.vetorDemanda = vetorDemanda;
		
		this.iniciarVetores(this.matrizImpedancia.get(0).size());
	}
	
	public void executarLadderIterative(){
		Complex corrente = Complex.ZERO;
		int contadorIteracoes = 0;
		
		Double diferencaAnterior = new Double(0);
		
		/**
		 * Aloca tensão nominal nos nós
		 * folhas do grafo e calcula o graus
		 * dos nós que compõem a árvore
		 */
		this.alocarTensaoNominal();
//		this.imprimeConjuntoLadder();
//		this.imprimeTensoes();
		
		/**
		 * Copia os graus calculados na função 
		 * alocarTensaoNominal() para graus
		 */
		this.setGraus((ArrayList<Integer>) this.getGrausIteracao().clone());
		
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);
		ComplexFormat cf = new ComplexFormat(nf);
		
//		System.out.println("Iniciou Fluxo de Potência");
		/**
		 * Inicia o calculo do fluxo de potência
		 */
		while(true){
//			for(int i = 0; i < this.grausIteracao.size(); i++)
//				System.out.println("Barra " + i + ": " + this.grausIteracao.get(i));
			
			/**
			 * FORWARD
			 *
			 * Iterador que varrerá
			 * o grafo do sistema elétrico
			 */
			for(int contador = 0; contador < this.getTensao().size(); contador++){
				
				corrente = Complex.ZERO;
				
				/**
				 * Se o nó do grafo do sistema elétrico
				 * não for o nó da subestação e estiver 
				 * com grau 1 para propósitos de avaliação
				 * entra no cálculo
				 */
				if((contador != this.getNoFeeder()) && (this.getGrausIteracao().get(contador) == 1)){
					
					/**
					 * Se o grau for igual a 1
					 * significa que é um nó folha
					 * que terá sua corrente no nó
					 * e no trecho calculada
					 */
					if(graus.get(contador) == 1){
//						System.out.println("Calculou tensões e correntes do agente " + contador);
						corrente = this.getVetorDemanda().get(contador);
						corrente = corrente.divide(this.getTensao().get(contador));
						corrente = corrente.conjugate();
						
						this.getCorrentesEletricasBarras().set(contador, corrente);
						this.calculoCorrenteTrecho(contador, corrente);
					}
					
					/**
					 * Se o grau for diferente de 1
					 * significa que é um nó interno
					 * que terá suas corrente no nó e
					 * no trecho calculadas apropriadamente
					 */
					if(graus.get(contador) != 1){
//						System.out.println("Calculou tensões e correntes do agente " + contador);
						this.calculoTensaoAgenteInterno(contador);
						
						corrente = this.getVetorDemanda().get(contador);
						corrente = corrente.divide(this.getTensao().get(contador));
						corrente = corrente.conjugate();
						
						this.getCorrentesEletricasBarras().set(contador, corrente);
						
						/**
						 * Código que percorre a vizinhança
						 * do nó interno e soma as correntes
						 * que chegam nele
						 */
						for(int contadorVizinhos = 0; contadorVizinhos < this.grausIteracao.size(); contadorVizinhos++){
							
							if(!(this.getMatrizImpedancia().get(contador).get(contadorVizinhos).equals(Complex.ZERO)) &&
									(this.grausIteracao.get(contadorVizinhos).equals(0))){
								
								corrente = corrente.add(this.getCorrentesEletricasTrechos().get(contador).get(contadorVizinhos));
							}
							
						}
						
						this.calculoCorrenteTrecho(contador, corrente);
					}
					
					contador = 0;
					
				}else{
					continue;
				}
				
			}
			
			/**
			 * Terminada a etapa FORWARD
			 * calcula-se a tensão do nó
			 * na subestação
			 */
			this.calculoTensaoAgenteInterno(noFeeder);
			
			/**
			 * Seta-se o grau do nó na
			 * subestação como 1
			 */
			this.grausIteracao.set(this.getNoFeeder(), 1);
			
//			System.out.println("Backward iteracao " + numIteracao);
//			this.imprimeTensoes();
//			this.imprimeCorrentesEletricasBarras();
//			this.imprimeConjuntoLadder();
			
//			System.out.println("Tolerância: " + tolerancia.getReal() +
//					" Diferença: " + Math.abs(tensaoNominal.getReal() - this.getTensao().get(noFeeder).getReal()) +
//					" Iteração: " + numIteracao);
			
//			try {
////				System.in.read();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
//			for(int contador = 0; contador < this.getCorrentesEletricasAgentes().size(); contador++){
//				System.out.println("Corrente agente" + contador + ": " + cf.format(this.getCorrentesEletricasAgentes().get(contador)));
//			}
//			
//			for(int contadorVertical = 0; contadorVertical < this.getCorrentesEletricasTrechos().get(0).size(); contadorVertical++){
//				for(int contadorHorizontal = 0; contadorHorizontal < this.getCorrentesEletricasTrechos().get(0).size(); contadorHorizontal++){
//					System.out.print(cf.format(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal)) + "    ");
//				}
//				System.out.println();
//			}
//			this.imprimeTensoes();
			/**
			 * Verifica se a tensão obtida na subestação
			 * está dentro da tolerância para o cálculo
			 * do fluxo de potência
			 */
//			System.out.println(Math.abs(tensaoNominal.abs() - this.getTensao().get(noFeeder).abs()) + " " + tolerancia.abs());
			if(Math.abs(tensaoNominal.abs() - this.getTensao().get(noFeeder).abs()) <= tolerancia.abs()){
				break;
			}
			
			/**
			 * BACKWARD
			 *
			 * Caso não, seta-se a tensão no nó da subestação
			 * para a tensão nominal e inicia-se o processo
			 * BACKWARD
			 */
			this.getTensao().set(this.getNoFeeder(), this.getTensaoNominal());
			
			/**
			 * Função que corrigirá as tensões
			 * na etapa BACKWARD
			 */
			this.corrigirTensoes(this.getNoFeeder());
			
//			System.out.println("Forward iteracao " + numIteracao);
//			this.imprimeTensoes();
			
			numIteracao++;
//			try {
//				System.in.read();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
//			if(Math.abs(tensaoNominal.getReal() - this.getTensao().get(noFeeder).getReal() - diferencaAnterior) <= 0.0000001){
//				this.corrigirInviabilidade();
//			}
			
			diferencaAnterior = Math.abs(tensaoNominal.getReal() - this.getTensao().get(noFeeder).getReal());
			
//			System.out.println("Tolerância: " + tolerancia.getReal() +
//					" Diferença: " + Math.abs(tensaoNominal.getReal() - this.getTensao().get(noFeeder).getReal()) +
//					" Diferença para verificar inviabilidade: " + diferencaAnterior);
				
//			this.imprimeTensoes();
//			this.imprimeConjuntoLadder();
			this.setGrausIteracao((ArrayList<Integer>) graus.clone());
			
			if(numIteracao >= 10000){
				//this.feeder.zerarCaminhoParaCalculoFluxo(this.getConjuntoLadder().size());
				break;
			}
			
//			if(contadorIteracoes == 9){
//				break;
//			}else{
//				contadorIteracoes++;
//				continue;
//			}
//			this.imprimeTensoes();
//			this.imprimeCorrentesEletricasBarras();
		}
//		System.out.println("Iterações: " + numIteracao);
//		this.imprimeTensoes();
		Complex perdaTotal = Complex.ZERO;
		for(int contadorVertical = 0; contadorVertical < this.getMatrizImpedancia().get(0).size(); contadorVertical++){
			for(int contadorHorizontal = 0; contadorHorizontal < this.getMatrizImpedancia().get(0).size(); contadorHorizontal++){
				
				if(contadorVertical <= contadorHorizontal){
					continue;
				}
				
//				Complex perda = this.getMatrizImpedancia().get(contadorVertical).get(contadorHorizontal).
//						multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal).
//						multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal)));
//				perdaTotal = perdaTotal.add(perda);
//				System.out.println(contadorVertical + " - " + contadorHorizontal + ": " + perda.getReal());
				
				perdasEletricasTotais = perdasEletricasTotais.add(this.getMatrizImpedancia().get(contadorVertical).get(contadorHorizontal).
						multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal).
						multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal))));
								
//				perdasEletricasTotais = perdasEletricasTotais.add(this.getMatrizImpedancia().get(contadorVertical).get(contadorHorizontal).
//						multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal).
//								multiply(this.getCorrentesEletricasTrechos().get(contadorVertical).get(contadorHorizontal))));
				
			}
			
		}
		
//		System.out.println("Perdas Elétricas totais: " + cf.format(perdasEletricasTotais));
//		System.out.println(this.formaPolar(perdasEletricasTotais));
	}
	
	/**
	 * Não me recordo do uso da
	 * função abaixo
	 */
// 	private void corrigirInviabilidade(){
// 		
// 		ArrayList<Integer> nosVerificarCoalisao = new ArrayList<Integer>();
// 		
// 		for(int contador = 0; contador < this.getGraus().size(); contador++){
// 			
// 			if(this.getTensao().get(contador).getReal() < 0){
// 				nosVerificarCoalisao.add(contador);
// 				
// 				System.out.println("no menor com tensão menor que 0: " + contador + " tensao real: " + this.getTensao().get(contador).getReal());
// //				try {
// //					System.in.read();
// //				} catch (IOException e) {
// //					// TODO Auto-generated catch block
// //					e.printStackTrace();
// //				}
// 			}
// 		}
// 		
// 		if(nosVerificarCoalisao.size() != 0){
// 			for(int contCoalisao = 0; contCoalisao < feeder.getCoalisoes().size(); contCoalisao++){
// 				
// 				for (Integer elemento : feeder.getCoalisoes().get(contCoalisao).getAgentesDaCoalisao()) {
// 					
// 					if(nosVerificarCoalisao.contains(elemento)){
// 						
// 						/**
// 						 * Zerar grafo e ligações
// 						 */
// 						for(Integer contElemento : feeder.getCoalisoes().get(contCoalisao).getAgentesDaCoalisao()){
// 							
// 							this.getTensao().set(contElemento, Complex.ZERO);
// 							this.getGraus().set(contElemento, 0);
// 							
// 							for(int cont = 0; cont < this.getMatrizImpedancia().get(0).size(); cont++){
// 								this.getMatrizImpedancia().get(contElemento).set(cont, Complex.ZERO);
// 								this.getMatrizImpedancia().get(cont).set(contElemento, Complex.ZERO);
// 							}
// 						}
// 						
// 						/**
// 						 * Indicar no caminho elétrico há uma inviabilidade com a coalisão
// 						 * seta-se as perdas elétricas como -1, uma flag
// 						 */
// 						for(int cont = 0; cont < feeder.getCaminhosEletricos().size(); cont++){
// 							
// 							if(feeder.getCaminhosEletricos().get(cont).getLiderCoalisao() ==
// 									feeder.getCoalisoes().get(contCoalisao).getLider()){
// 								
// 								feeder.getCaminhosEletricos().get(cont).setPerdaEletricaNoCaminho(new Complex(-1, 0));
// 								
// 								break;
// 							}
// 						}
// 						
// 						nosVerificarCoalisao.remove(elemento);
// 						break;
// 						
// 					}
// 					
// 				}
// 				
// 			}
// 		}
// 		
// 	}
	
	/**
	 * Corrige as tensões durante
	 * a etapa BACKWARD do algoritmo
	 */
	private void corrigirTensoes(int numVerificado){
		Complex tensao = Complex.ZERO;
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(8);
		ComplexFormat cf = new ComplexFormat(nf);
		
		if(this.grausIteracao.get(numVerificado) == 1){
			
			/**
			 * Estrutura que varrerá o grafo completo
			 */
			for(int contadorMatriz = 0; contadorMatriz < this.getMatrizImpedancia().get(0).size(); contadorMatriz++){
				
				if(contadorMatriz ==  numVerificado){
					continue;
				}
				
				/**
				 * Se a condição abaixo for verificada
				 * significa que o nó não faz parte da 
				 * árvore do sistema elétrico ativo
				 */
				if((this.getMatrizImpedancia().get(numVerificado).get(contadorMatriz).equals(Complex.ZERO) ||
						(this.grausIteracao.get(contadorMatriz) != 0))){

					continue;
				} else{
					
					/**
					 * Cálculo da nova tensão
					 */
					tensao = this.getTensao().get(numVerificado).subtract(
							this.getMatrizImpedancia().get(numVerificado).get(contadorMatriz).multiply(
									this.getCorrentesEletricasTrechos().get(numVerificado).get(contadorMatriz)));
					
//					System.out.println("Tensão agentes " + numVerificado + " " + contadorMatriz +
//							"\nCorrente trecho(" + numVerificado + ")(" + contadorMatriz + "): " + this.formaPolar(this.getCorrentesEletricasTrechos().get(numVerificado).get(contadorMatriz)) +
//							" || " + cf.format(this.getCorrentesEletricasTrechos().get(numVerificado).get(contadorMatriz)) +
//							"\nMatriz Impedância(" + numVerificado + ")(" + contadorMatriz + "): " + this.formaPolar(this.getMatrizImpedancia().get(numVerificado).get(contadorMatriz)) +
//							" || " + cf.format(this.getMatrizImpedancia().get(numVerificado).get(contadorMatriz)) +
//							"\nTensão(" + numVerificado + "): " + this.formaPolar(this.getTensao().get(numVerificado)) +
//							" || " + cf.format(this.getTensao().get(numVerificado)) +
//							"\nTensão Calculada(" + contadorMatriz + "): " + this.formaPolar(tensao) +
//							" || " + cf.format(tensao) + "\n");
					/**
					 * Configuração da nova tensão calculada
					 */
					this.getTensao().set(contadorMatriz, tensao);
					
					/**
					 * ???
					 */
					this.grausIteracao.set(contadorMatriz, 1);
					
					/**
					 * Chamada recursiva para
					 * correção das tensões
					 */
					this.corrigirTensoes(contadorMatriz);
				}
			}
			
		}
	}
	
	private void iniciarVetores(Integer ladoMatriz){
		
		for(int contador = 0; contador < ladoMatriz; contador++){
			this.correntesEletricasBarras.add(Complex.ZERO);
			this.tensao.add(Complex.ZERO);
			this.grausIteracao.add(new Integer(0));
		}
		
		for(int contador = 0; contador < ladoMatriz; contador++){
			this.correntesEletricasTrechos.add((ArrayList<Complex>) this.correntesEletricasBarras.clone());
		}
		
	}
	
	/**
	 * Aloca tensão nominal nos nós
	 * folhas do grafo e calcula o graus
	 * dos nós que compõem a árvore
	 */
	private void alocarTensaoNominal(){
		
		int ladoMatriz= this.matrizImpedancia.get(0).size();
		int flag = 0;
		
		for(int contadorVertical = 0; contadorVertical < ladoMatriz; contadorVertical++){
			for(int contadorHorizontal = 0; contadorHorizontal < ladoMatriz; contadorHorizontal++){
				
				if(!this.getMatrizImpedancia().get(contadorVertical).get(contadorHorizontal).equals(Complex.ZERO)){
					flag = flag + 1;
				}
				
			}
			
			this.grausIteracao.set(contadorVertical, flag);
			
			if(flag == 1){
				this.tensao.set(contadorVertical, tensaoNominal);
			}
			
			flag = 0;
		}
		
	}
	
	/**
	 * Calcula corrente no trecho
	 */
	private void calculoCorrenteTrecho(int num, Complex corrente){
		this.getGrausIteracao().set(num, this.getGrausIteracao().get(num) - 1);
		
		for(int contador = 0; contador < this.getMatrizImpedancia().get(0).size(); contador++){
			
			if((this.getMatrizImpedancia().get(num).get(contador).equals(Complex.ZERO)) ||
					(this.getGrausIteracao().get(contador) <= 0)){
				
				continue;
				
			}else{
				this.getGrausIteracao().set(contador, this.getGrausIteracao().get(contador) - 1);
				
				this.getCorrentesEletricasTrechos().get(num).set(contador, corrente);
				this.getCorrentesEletricasTrechos().get(contador).set(num, corrente);
			}
			
		}
	}
	
	/**
	 * Calcula a tensão de um nó
	 * que não é folha do grafo
	 */
	private void calculoTensaoAgenteInterno(int num){
		
		Complex tensaoTemp = Complex.INF;
	
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);
		ComplexFormat cf = new ComplexFormat(nf);
		
		for(int contador = 0; contador < this.getMatrizImpedancia().get(0).size(); contador++){
			
			if((this.getMatrizImpedancia().get(num).get(contador).equals(Complex.ZERO)) ||
					(this.getGrausIteracao().get(contador) != 0)){
				
				continue;
				
			}else{
				
//				if((this.getTensao().get(contador)
//						.add(this.getMatrizImpedancia().get(num).get(contador)
//						.multiply(this.getCorrentesEletricasTrechos().get(num).get(contador)))).abs() < tensaoTemp.abs()) {
					
				tensaoTemp = this.getTensao().get(contador)
						.add(this.getMatrizImpedancia().get(num).get(contador)
						.multiply(this.getCorrentesEletricasTrechos().get(num).get(contador)));
//				}
				
//				System.out.println("Tensão agentes " + contador + " " + num +
//						"\nCorrente trecho(" + contador + ")(" + num + "): " + this.formaPolar(this.getCorrentesEletricasTrechos().get(contador).get(num)) +
//						" || " + cf.format(this.getCorrentesEletricasTrechos().get(contador).get(num)) +
//						"\nMatriz Impedância(" + contador + ")(" + num + "): " + this.formaPolar(this.getMatrizImpedancia().get(contador).get(num)) +
//						" || " + cf.format(this.getMatrizImpedancia().get(contador).get(num)) +
//						"\nTensão(" + contador + "): " + this.formaPolar(this.getTensao().get(contador)) +
//						" || " + cf.format(this.getTensao().get(contador)) +
//						"\nTensão Calculada(" + num + "): " + this.formaPolar(tensaoTemp) +
//						" || " + cf.format(tensaoTemp) + "\n");
			}
		}
		
//		System.out.println("Tensão nova para " + num + ": " + cf.format(tensaoTemp));
		this.getTensao().set(num, tensaoTemp);
	}
	
	private String formaPolar(Complex complex){
		double modulo = Math.sqrt(Math.pow(complex.getReal(), 2) + Math.pow(complex.getImaginary(), 2));
		double fase = (Math.atan(complex.getImaginary() / complex.getReal()) * 180) / Math.PI;
		
		String saida = modulo + " | " + fase;
		
		return saida;
	}
	
	public void imprimeTensoes(){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);
		ComplexFormat cf = new ComplexFormat(nf);

		System.out.println("-- Tensões: ");
		
		for(int contador = 0; contador < this.getTensao().size(); contador++){
			System.out.println("Barra " + contador + ": " + this.getTensao().get(contador).getReal());
		}
	}
	
	public void imprimeGrausIteracao(){
		
		System.out.println("-- Graus Iteração: ");
		
		for(int contador = 0; contador < this.getTensao().size(); contador++){
			System.out.println("Barra " + contador +  ": " + this.getGrausIteracao().get(contador));
		}
	}
	
	public void imprimeCorrentesEletricasBarras(){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);
		ComplexFormat cf = new ComplexFormat(nf);

		System.out.println("-- Correntes Elétricas nas Barras: ");
		
		for(int contador = 0; contador < this.getTensao().size(); contador++){
			System.out.println("Barra " + contador +  ": " + this.formaPolar(this.getCorrentesEletricasBarras().get(contador)) + 
					" | " + cf.format(this.getCorrentesEletricasBarras().get(contador)));
		}
	}
	
	public ArrayList<Integer> getGraus() {
		return graus;
	}

	public void setGraus(ArrayList<Integer> graus) {
		this.graus = graus;
	}

	public Integer getNoFeeder() {
		return noFeeder;
	}

	public void setNoFeeder(Integer noFeeder) {
		this.noFeeder = noFeeder;
	}

	public Integer getNumIteracao() {
		return numIteracao;
	}

	public void setNumIteracao(Integer numIteracao) {
		this.numIteracao = numIteracao;
	}

	public Complex getTensaoNominal() {
		return tensaoNominal;
	}

	public void setTensaoNominal(Complex tensaoNominal) {
		this.tensaoNominal = tensaoNominal;
	}

	public Complex getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(Complex tolerancia) {
		this.tolerancia = tolerancia;
	}

	public ArrayList<Complex> getVetorDemanda() {
		return vetorDemanda;
	}

	public void setVetorDemanda(ArrayList<Complex> vetorDemanda) {
		this.vetorDemanda = vetorDemanda;
	}

	public ArrayList<Integer> getGrausIteracao() {
		return grausIteracao;
	}

	public void setGrausIteracao(ArrayList<Integer> grausIteracao) {
		this.grausIteracao = grausIteracao;
	}

	public ArrayList<ArrayList<Complex>> getMatrizImpedancia() {
		return matrizImpedancia;
	}

	public void setMatrizImpedancia(ArrayList<ArrayList<Complex>> matrizImpedancia) {
		this.matrizImpedancia = matrizImpedancia;
	}
	
	public ArrayList<ArrayList<Complex>> getCorrentesEletricasTrechos() {
		return correntesEletricasTrechos;
	}

	public void setCorrentesEletricasTrechos(
			ArrayList<ArrayList<Complex>> correntesEletricasTrechos) {
		this.correntesEletricasTrechos = correntesEletricasTrechos;
	}
	
	public ArrayList<Complex> getCorrentesEletricasBarras() {
		return correntesEletricasBarras;
	}

	public void setCorrentesEletricasBarras(
			ArrayList<Complex> correntesEletricasBarras) {
		this.correntesEletricasBarras = correntesEletricasBarras;
	}

	public ArrayList<ArrayList<Complex>> getPerdasEletricas() {
		return perdasEletricas;
	}

	public Complex getPerdasEletricasTotais() {
		return perdasEletricasTotais;
	}

	public void setPerdasEletricasTotais(Complex perdasEletricasTotais) {
		this.perdasEletricasTotais = perdasEletricasTotais;
	}

	public void setPerdasEletricas(ArrayList<ArrayList<Complex>> perdasEletricas) {
		this.perdasEletricas = perdasEletricas;
	}

	public ArrayList<Complex> getTensao() {
		return tensao;
	}

	public void setTensao(ArrayList<Complex> tensao) {
		this.tensao = tensao;
	}
}
