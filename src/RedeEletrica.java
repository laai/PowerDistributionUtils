import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.commons.math.complex.Complex;
import org.apache.commons.math.complex.ComplexFormat;

public class RedeEletrica {

	private Complex tensaoNominal = Complex.ZERO;
	
	private Integer numBarras = new Integer(0);
	
	private ArrayList<ArrayList<Complex>> redeEletrica = new ArrayList<ArrayList<Complex>>();
	
	private ArrayList<Complex> cargas = new ArrayList<Complex>();
	
	public RedeEletrica(){
		this.lerRedeEletrica();
		this.lerCargas();
	}
	
	public RedeEletrica(Complex tensaoNominal){
		this.tensaoNominal = tensaoNominal;
		this.lerRedeEletrica();
		this.lerCargas();
	}
	
	/**
	 * Lê a matriz do sistema de distribuição e atribui para matrizSistema
	 */
	public void lerRedeEletrica() {

		ArrayList<Complex> recebeArray = new ArrayList<Complex>();

		try {
			/*
			 * Para contar o lado da matriz da rede elétrica
			 * 
			 * Espera-se que a matriz sempre seja quadrada!
			 * 
			 * TODO fazer uma rotina para verificar se a matriz é quadrada; caso
			 * não, abortar a execução do programa
			 */
			File contarLado = new File("sistema_real.txt");
			Scanner scanner_contarLado = new Scanner(contarLado);
			
			Integer ladoMatriz = 0;
			while (scanner_contarLado.hasNextLine()) {
			    ladoMatriz++;
			    scanner_contarLado.nextLine();
			}
			
			/*
			 * O arquivo com o sistema de distribuição sempre se chamará
			 * sistema.txt
			 */
			File arquivo_real = new File("sistema_real.txt");
			Scanner scanner_real = new Scanner(arquivo_real);

			File arquivo_imag = new File("sistema_imag.txt");
			Scanner scanner_imag = new Scanner(arquivo_imag);
			
			// System.out.println("ladoMatriz: " + ladoMatriz);
			/*
			 * Rotina para criar o ArrayList bi-dimensional adicionando
			 * ArrayList por ArrayList à variável matrizSistema
			 */
			for (int contadorHorizontal = 0; contadorHorizontal < ladoMatriz; contadorHorizontal++) {
				for (int contadorVertical = 0; contadorVertical < ladoMatriz; contadorVertical++) {
					
					String d1 = scanner_real.next();
					String d2 = scanner_imag.next();
					
					recebeArray.add(new Complex(new Double(d1), new Double(d2)));

				}

				this.getRedeEletrica().add((ArrayList<Complex>) recebeArray.clone());
				recebeArray.clear();
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * Verificação - imprime dados de matrizSistema para conferência manual
	 */
	public void imprimeRedeEletrica() {

		ComplexFormat cf = new ComplexFormat();

		System.out.println("Lado da Matriz Alocada: "
				+ redeEletrica.get(0).size());
		for (int contadorHorizontal = 0; contadorHorizontal < redeEletrica
				.get(0).size(); contadorHorizontal++) {
			for (int contadorVertical = 0; contadorVertical < redeEletrica
					.get(0).size(); contadorVertical++) {

				System.out.print(cf.format(redeEletrica.get(
						contadorHorizontal).get(contadorVertical))
						+ "  ");

			}

			System.out.println();
		}
	}
	
	public void imprimeRedeResistencia() {

		for (int contadorHorizontal = 0; contadorHorizontal < redeEletrica
				.get(0).size(); contadorHorizontal++) {
			for (int contadorVertical = 0; contadorVertical < redeEletrica
					.get(0).size(); contadorVertical++) {

				System.out.print(redeEletrica.get(
						contadorHorizontal).get(contadorVertical).getReal()
						+ "  ");

			}

			System.out.println();
		}
	}

	public void lerCargas(){
		try{
			File contarLado = new File("demanda_real.txt");
			Scanner scanner_contarLado = new Scanner(contarLado);
			
			Integer ladoMatriz = 0;
			while (scanner_contarLado.hasNextLine()) {
			    ladoMatriz++;
			    scanner_contarLado.nextLine();
			}
			
			File arquivo_real = new File("demanda_real.txt");
			Scanner scanner_real = new Scanner(arquivo_real);
	
			File arquivo_imag = new File("demanda_imag.txt");
			Scanner scanner_imag = new Scanner(arquivo_imag);
			
			// System.out.println("ladoMatriz: " + ladoMatriz);
			/*
			 * Rotina para criar o ArrayList bi-dimensional adicionando
			 * ArrayList por ArrayList à variável matrizSistema
			 */
			for (int contador = 0; contador < ladoMatriz; contador++) {
				String d1 = scanner_real.next();
				String d2 = scanner_imag.next();
				
				this.getCargas().add(new Complex(new Double(d1) * 1000,
						new Double(d2) * 1000));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void imprimeCargas() {
		ComplexFormat cf = new ComplexFormat();
		
		for (int contador = 0; contador < cargas.size(); contador++) {
			System.out.print(cf.format(cargas.get(contador)) + "  ");
		}

		System.out.println();
	}

	public Complex getTensaoNominal() {
		return tensaoNominal;
	}

	public void setTensaoNominal(Complex tensaoNominal) {
		this.tensaoNominal = tensaoNominal;
	}

	public Integer getNumBarras() {
		return numBarras;
	}

	public void setNumBarras(Integer numBarras) {
		this.numBarras = numBarras;
	}

	public ArrayList<Complex> getCargas() {
		return cargas;
	}

	public void setCargas(ArrayList<Complex> cargas) {
		this.cargas = cargas;
	}
	
	public ArrayList<ArrayList<Complex>> getRedeEletrica() {
		return redeEletrica;
	}

	public void setRedeEletrica(ArrayList<ArrayList<Complex>> redeEletrica) {
		this.redeEletrica = redeEletrica;
	}
}
