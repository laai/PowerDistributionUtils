#!/bin/python

import sys
import numpy
import random

from numpy import *

f = open(sys.argv[1], "r")
s = f.readlines()

numNodos = int(sys.argv[2]) + int(sys.argv[3])

m_real = numpy.zeros((numNodos, numNodos))
m_imag = numpy.zeros((numNodos, numNodos))
m_demanda_real = numpy.zeros(numNodos)
m_demanda_imag = numpy.zeros(numNodos)

cont = 0
for linha in s:
	linha = linha.split(' ')
	m_real[int(linha[1])][int(linha[2])] = linha[3]
	m_real[int(linha[2])][int(linha[1])] = linha[3]
	
	m_imag[int(linha[1])][int(linha[2])] = linha[4]
	m_imag[int(linha[2])][int(linha[1])] = linha[4]

cont = int(sys.argv[3])
for linha in s:
	linha = linha.split(' ')
	m_demanda_real[cont] = linha[5]
	m_demanda_imag[cont] = linha[6]
	cont += 1
	if cont == numNodos:
	    break

savetxt("sistema_real.txt", m_real, fmt="%.4G")
savetxt("sistema_imag.txt", m_imag, fmt="%.4G")
savetxt("demanda_real.txt", m_demanda_real)
savetxt("demanda_imag.txt", m_demanda_imag)
